module ManageIQ
  module Automate
    module Async
      module Workers
        class Generic_worker
          def initialize(handle = $evm)
            @handle = handle
          end

          def manage_active_task(name, request)
            $evm.log(:info, "CC -manage #{name} => #{request}")
            ManageIQ::Automate::Async::Utils::Utils.update_task_active(name )
            do_something(name, request)
          end
          
          def do_something(name, request)
            @handle.log(:info, "CC -managing #{request}")
            get_ip(name, request) if request["params"]["action"] == "get"
            release_ip(name, request) if request["params"]["action"] == "release"
          end
          
          def release_ip(name, request)
            network=request["params"]["network"] rescue nil
            netmask=request["params"]["netmask"] rescue nil
            ip=request["return_values"] rescue nil
            if network.blank? || netmask.blank? || ip.blank?
              $evm.log(:error,"no network definition or IP")
              ManageIQ::Automate::Async::Utils::Utils.update_task_error(name , "no network definition or IP")
            else
              cache_name="#{network}/#{netmask}"
              allocated_ips=ManageIQ::Automate::Async::Utils::Utils.get_from_cache(cache_name) || []
              allocated_ips.delete(ip.to_s)
              $evm.log(:info,"ip #{ip} released")
              ManageIQ::Automate::Async::Utils::Utils.update_task_complete(name , nil)
            end
          end
          
          def get_ip(name, request)
            require "ipaddr"
            network=request["params"]["network"] rescue nil
            netmask=request["params"]["netmask"] rescue nil
            if network.blank? || netmask.blank?
              $evm.log(:error,"no network definition")
              ManageIQ::Automate::Async::Utils::Utils.update_task_error(name , "no network definition")
            else
              cache_name="#{network}/#{netmask}"
              allocated_ips=ManageIQ::Automate::Async::Utils::Utils.get_from_cache(cache_name)
              allocated_ips = {} if allocated_ips.blank?
              $evm.log(:info,"#{cache_name} allocated ips = #{allocated_ips}")
              
              lan=IPAddr.new("#{network}/#{netmask}").to_range
              free_ips=lan.select{ |ip| allocated_ips[ip.to_s].blank? && ip != lan.first && ip != lan.last }
              if free_ips.count == 0
                $evm.log(:error,"no IP left")
                ManageIQ::Automate::Async::Utils::Utils.update_task_error(name , "no IP left")
              else
                ip=free_ips.first
                $evm.log(:info,"ip #{ip} allocated")
                allocated_ips[ip.to_s]="allocated"

                ManageIQ::Automate::Async::Utils::Utils.add_to_cache(cache_name,allocated_ips)
                ManageIQ::Automate::Async::Utils::Utils.update_task_finished(name , ip.to_s)
              end
            end
          end
          
          def main
            return unless ManageIQ::Automate::Async::Utils::Utils.elected_worker?(@handle)
            ManageIQ::Automate::Async::Utils::Utils.set_elected_worker(@handle)
            tasks=ManageIQ::Automate::Async::Utils::Utils.schedule_tasks(@handle)
            tasks.each { |name, task| manage_active_task(name, task)}

            ManageIQ::Automate::Async::Utils::Utils.retry_or_die(@handle, tasks.count!=0)
          end
        end
      end
    end
  end
end

ManageIQ::Automate::Async::Workers::Generic_worker.new.main
