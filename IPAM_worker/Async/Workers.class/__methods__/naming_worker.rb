module ManageIQ
  module Automate
    module Async
      module Workers
        class Naming_worker
          # INPUTS: prefix [String]
          # output: fullname [String]
          def initialize(handle = $evm)
            @handle = handle
          end

          def manage_active_task(name, request)
            $evm.log(:info, "CC -manage #{name} => #{request}")
            ManageIQ::Automate::Async::Utils::Utils.update_task_active(name )
            do_something(name, request)
          end
          
          def do_something(name, request)
            req_params=request["params"]
            max_id=36*36
            vm_name="***"
            vm_prefix=req_params["prefix"] || "cfme"
            last_vm_id=(ManageIQ::Automate::Async::Utils::Utils.get_from_cache(vm_prefix) || 0).to_i
            $evm.log(:info,"CC- setting name for #{vm_prefix} with last id #{last_vm_id}")
            next_id=0
            begin 
              next_id+=1
              vm_id = last_vm_id + next_id
              vm_suffix=vm_id.modulo(max_id).to_s(36).rjust(2, "0")
              vm_name="#{vm_prefix}#{vm_suffix}".upcase
              $evm.log(:info,"  CC- checking name for #{vm_id} => #{vm_name}")
            #end while ($evm.vmdb(:vm).where( :name => "#{vm_name}", :orphaned => false, :archived => false).count >0 || next_id == max_id - 1)
              end while ($evm.vmdb(:vm).where( :name => "#{vm_name}").count >0 || next_id == max_id - 1)
            if (next_id == max_id - 1)
              $evm.log(:info,"CC- no more free ID for #{vm_prefix}")
              ManageIQ::Automate::Async::Utils::Utils.update_task_error(name, request , "no more IDs")
            else
              ManageIQ::Automate::Async::Utils::Utils.add_to_cache(vm_prefix,vm_id.to_i)
              $evm.log(:info,"CC- setting name for #{vm_name} : #{vm_prefix} as #{vm_id}")

              ManageIQ::Automate::Async::Utils::Utils.update_task_finished(name, {"name" => vm_name})
            end
          end
          
          def main
            return unless ManageIQ::Automate::Async::Utils::Utils.elected_worker?(@handle)
            ManageIQ::Automate::Async::Utils::Utils.set_elected_worker(@handle)
            tasks=ManageIQ::Automate::Async::Utils::Utils.schedule_tasks(@handle)
            tasks.each { |name, task| manage_active_task(name, task)}
            
            ManageIQ::Automate::Async::Utils::Utils.retry_or_die(@handle, tasks.count!=0)
          end
        end
      end
    end
  end
end

ManageIQ::Automate::Async::Workers::Naming_worker.new.main
