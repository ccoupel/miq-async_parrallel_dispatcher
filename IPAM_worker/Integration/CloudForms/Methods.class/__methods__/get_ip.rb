#
# Description: <Method description here>
#
  prov=$evm.root[$evm.root["vmdb_object_type"]]
  network=prov.get_dialog_option("dialog_option_0_network") rescue nil
  netmask=prov.get_dialog_option("dialog_option_0_netmask") rescue nil
  $evm.log(:info,"CC- network=#{network} netmask=#{netmask}")
  request=ManageIQ::Automate::Async::Utils::Utils.add_async_task("new_ip", {"action" => "get", 
                                                                            "network" => network, 
                                                                            "netmask" => netmask}, type="IPAM",prov)
  $evm.log(:info,"CC- request=#{request}")
