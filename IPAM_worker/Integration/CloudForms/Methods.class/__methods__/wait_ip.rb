# / Infra / VM / Provisioning / Naming / default (vmname)

#
# Description: This is the default naming scheme
# 1. Wait for the asynchrone new name
# 2. set the naming options
#
module ManageIQ
  module Automate
    module Infrastructure
      module VM
        module Provisioning
          module Naming
            class VmName
              def initialize(handle = $evm)
                @handle = handle
              end

              def main
                prov=@handle.root[$evm.root["vmdb_object_type"]]
                @handle.root["ae_retry_interval"]=20.seconds
                @handle.root["ae_result"]="retry"
                
                merged_queue=ManageIQ::Automate::Async::Utils::Utils.synchronize_tasks_from_worker("IPAM",@handle)
                $evm.log(:info,"merged queue=#{merged_queue}")
                task=ManageIQ::Automate::Async::Utils::Utils.get_async_task("new_ip", "IPAM",prov)
                @handle.log(:info, "  CC- waiting task=#{task}.")
                unless task.nil?
                  case task["state"].downcase
                  when 'error'
                    @handle.log(:info, "  CC- Async_worker is in ERROR.")
                    @handle.root["ae_result"]="error"
                  when 'finished'
                    if task["status"].downcase == "error"
                    then
                      @handle.log(:info, "  CC- New IP is with error: #{task["return_values"]}. Exiting.")
                      @handle.root["ae_result"]="error"
                    else
                      @handle.log(:info, "  CC- New IP is finished provisioned : #{task["return_values"]}. Exiting.")
                      @handle.root["ae_result"]="ok"
                      ManageIQ::Automate::Async::Utils::Utils.update_task_complete("new_ip","done")
                    end
                  when 'complete'
                      @handle.log(:info, "  CC- New IP already set: #{task["return_values"]}. Exiting.")
                      @handle.root["ae_result"]="ok"
                  else
                    @handle.log(:info, "  CC- Resource is #{task["state"]}:#{task["status"]}. Retrying in #{@handle.root["ae_retry_interval"]} seconds.")
                      @handle.root['ae_retry_interval'] = '20.seconds'
                    @handle.root["ae_result"]="retry"
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  begin
    ManageIQ::Automate::Infrastructure::VM::Provisioning::Naming::VmName.new.main
    #$evm.root['ae_result']         = 'retry' #FOR DEBUG
  rescue Exception => e  
    $evm.log('info', "CC- ##################################################### ERROR RETRYING #########################")
    $evm.log('info', "CC- e=#{e.inspect}") rescue nil
    $evm.log(:error, "[#{e}]\n#{e.backtrace.join("\nERROR: ")}") rescue nil      
    $evm.root['ae_result']         = 'retry'
    $evm.root['ae_retry_interval'] = '20.seconds'
    exit MIQ_OK
  end
end
