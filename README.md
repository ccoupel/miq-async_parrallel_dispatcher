# Async_parallele_dispatcher
Access: confirmed
Fomat: datastore

## Problematics:
During provisioning, some limitations make the automation not so powerful. For example:
> - There is no semaphores to avoid race concurrence:
> >     How multiple provision can write to a single file without collision?
> - There is no shared memory between several provisioning, nor cache mechanism:
> >    How to make a vm_name based on a dynamique prefix and a specific counter for each prefix?
> - State machine runs steps one after the other; no parallel tasks can be run.
> > How to keep running the state machine while a step that takes long time to execute runs in background, until I need the result of this step.

## Solution:
The idea is to provide a mechanism like a micro-service,  that will work the same as a worker, but implemented with automation mechanism,that will solve these limitations. In the following document, the word “worker” refer to the implemented dispatcher:
> - Provisions state machines send a request to the worker to write to the file, The worker then writes to the file sequentially, avoiding that a write will erase the previous one.
> - Provision state machine ask for the last counter value for a given prefix, the worker then increase the counter
> - Provision state machine send a request to the worker to do a long time running task, then continues next tasks. When the execution of the task is required to continue, the provisioning state machine ask for the status of the task and wait until it is finished.

This generic worker is implemented only from the standard automation, no need of external solutions

## Constraints:
There are some constraints to have a reliable solution:
> 1. The worker must be unique, a singleton, only on instance must run at a time
> 1. Multiple workers can be run to do different tasks
> 1. Each tasks must have a unique identity, based on the source provisioning, the worker the task must be run with, a name to easily retrieve it.
> 1. The worker can’t write directly to the provision option as there is no synchronization between the `$evm.root[“miq_provision”]` and the `$evm.vmdb(:miq_provision,ID)`, so some pushed datas can be lost
> 1. The provision can not write to the worker options as several provision can ask for a request in the same time

## How to do it:
> 1. ### Singleton
The worker is a state machine run by an automation_request. Before starting a new instance of a worker type, we check that an other automation_task with the same instance_name is not already running.
But, as the instance of the worker is started with a new provisioning, with multiple provisioning, multiple worker can be started because when the check is done, none of them are already started. So, to ensure the singleton, each time the  main step of the worker is run, we check that there is no other worker running. If so, only the first of them is elected as MASTER, the others simply exit.

> 1. ### MIQ_Provision queue
The requests are stored in a queue in options attributes as a Hash. Each queue is named by the type of worker the requests are for:  `miq_provision.options[“_Async_Naming”]` for the worker “Naming”
A request is also a hash as: 
~~~~
<prov_id>_<request_name> => {
                        "name" => <request_name>,
                  "type" => <worker_type>,
                  "prov_id" => <prov.id>,
                  "state" => “pending",
                  "status" => "ok",
                  "return_values" => nil,
                  "params" => params}
~~~~
Params are also Hash and the structure is dependant of the worker type

So, the miq_provision, can have multiple queues for each needed workers, each queue can have multiple requests.

> 1. ### Worker Queue
The worker queue has the same structure, but contains the requests of all active miq_provision. It handle only one queue named with its own type ( “_Async_Naming” for the naming worker)

> 1. ### lifecycle
Workers have to be started for the provisioning to be complete, so it is started via automation_approved event and will run until there is active provisioning.
But, as the worker is in fact an automation request, the execution time is limited by the core itself. 
We will give our own TTL to ensure that we will never reach the automation timeout.

Once the TTL is reached, we check if there are more provision request running, if so, we launch a new worker and kill the previous one to ensure that there is always only on worker running.

While a worker is started or restarted, during initialisation, it looks for the previous dead master worker of its type and copy the requests_queue (options[“_ASYNC_Naming”]) and the cache values (options[“_cache_Async”])

The main step of the worker is started regularly and exits with a “retry” if the TTL is not reach and sleep for the specified time.

## How it works:
The communication between the provisioning state machine and the worker is done through the options of each. 
> 1. The provisioning set a request in its options and continues its steps
> > `ManageIQ::Automate::Async::Utils::Utils.add_async_task("new_vm_name", {"prefix" => vm_prefix}, type="Naming",  prov)`
> 1. The worker collect the requests and save them to its own options
> > `synchronize_tasks_from_prov`

> 1. The worker do the requested tasks and save the results to its own options as finished
> > `ManageIQ::Automate::Async::Utils::Utils.schedule_tasks(@handle)`
> 1. The provisioning check the options of the worker and update its local options with the finished ones
> > `ManageIQ::Automate::Async::Utils::Utils.synchronize_tasks_from_worker("Naming",@handle)`
> 1. The provisioning then can handle the results
> >  `ManageIQ::Automate::Async::Utils::Utils.update_task_complete("new_vm_name")`

## How to use a worker:
 - example to request a new VM_name with a specific counter based on vm_name prefix
> 1. Update the event instance to start your workers by including /Async/Workers/Start_Workers?_worker_type=Naming#create in /system/Event/RequestEvent/Request/request_approval
> 1. In any method step:
> > 1. Include /Async/Utils/Utils as embedded method to access the methods
> > 1. Add a request to request a new VM_name:
> > > ` vm_prefix = "#{trigramme}" + "#{car45}" + "#{xxx}" + "#{yy}
ManageIQ::Automate::Async::Utils::Utils.add_async_task("new_vm_name", {"prefix" => vm_prefix}, type="Naming",  prov)`
> 1. In an later method:
> > 1. Include /Async/Utils/Utils as embedded method to access the methods
> > 1. Synchronize the tasks with the worker:
> > > `ManageIQ::Automate::Async::Utils::Utils.synchronize_tasks_from_worker("Naming",@handle)`
> > 1. Retreave the task:
> > > ` task=ManageIQ::Automate::Async::Utils::Utils.get_async_task("new_vm_name", "Naming")`
> > 1. Manage task status:


~~~~
                unless task.nil?
                  case task["state"].downcase
                  when 'error'
                    @handle.log(:info, "  CC- Async_worker is in ERROR.")
                    @handle.root["ae_result"]="error"
                  when 'finished'
                    if task["status"].downcase == "error"
                    then
                        @handle.log(:info, "  CC- New name is provisioned but with error. Exiting.")
                        @handle.root["ae_result"]="error"
                    else
                        @handle.log(:info, "  CC- New name is finished provisioned : #{task["return_values"]}. Exiting.")
                        myvmname=task["return_values"]["name"] rescue ""
                        @handle.root["ae_result"]="ok"
                        $evm.object['vmname'] = myvmname.downcase
                      ManageIQ::Automate::Async::Utils::Utils.update_task_complete("new_vm_name")
                    end
                  when 'complete'
                      @handle.log(:info, "  CC- New name already set: #{task["return_values"]}. Exiting.")
                      @handle.root["ae_result"]="ok"
                  Else
~~~~
> > 1. Retry if task is not finished:
~~~~
                    @handle.log(:info, "  CC- Resource is #{task["state"]}:#{task["status"]}. Retrying in #{@handle.root["ae_retry_interval"]} seconds.")
                      @handle.root['ae_retry_interval'] = '20.seconds'
                    @handle.root["ae_result"]="retry"
                  End
~~~~

## How to write a worker:
> * Copy the generic worker method `/Async/Workers/Generic_worker` as `/Async/Workers/NEW_worker `
> * If the defaults in the .missing are not what you expect, copy the instance to `/Async/Workers/NEW_worker` and setup the values as wanted.
> * Update the do_something in the copied method to manage the request.
> * Finish the do_something with `ManageIQ::Automate::Async::Utils::Utils.update_task_finished(request , retrun_values_hash)`
Or `ManageIQ::Automate::Async::Utils::Utils.update_task_error(request , “error message”)`


## an administration Dialog is available in `dialog_export_Worker_Administrator.yml`