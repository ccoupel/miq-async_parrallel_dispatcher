#
# Description: <Method description here>
#

$evm.log(:info,"CC- get all tasks")
all_tasks=$evm.vmdb(:automation_task).all

$evm.log(:info,"CC- get all worker tasks form #{all_tasks.count} tasks")
workers={}
all_tasks.each{ |task| 
  if (task.options[:instance_name].include?"_worker" rescue false) && (task.state == "active")
    workers[ task.options[:instance_name]] = task.id
  end
}

$evm.log(:info,"CC- get all active worker names from #{workers} workers")
$evm.object["value"]=workers.to_yaml

$evm.log(:info,"CC- all worker names from #{$evm.object["value"]} workers")
