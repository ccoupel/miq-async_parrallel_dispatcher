#
# Description: <Method description here>
#

$evm.log(:info,"CC- get all tasks")
all_tasks=$evm.vmdb(:automation_task).all

$evm.log(:info,"CC- get all worker tasks form #{all_tasks.count} tasks")
tasks={}
all_tasks.each{ |task| 
  instance_name=task.options[:instance_name] rescue $evm.log(:error," task #{task.id} ==> #{task.options}")
  previous_id=tasks[ instance_name] || 0
  if (task.options[:instance_name].include?"_worker" rescue false) && (previous_id.to_i < task.id.to_i)
    tasks[ task.options[:instance_name]] = task.options[:instance_name]
  end
}

$evm.log(:info,"CC- get all worker names from #{tasks.count} workers")
$evm.object["values"]=tasks.sort

$evm.log(:info,"CC- all worker names from #{$evm.object["values"]} workers")
