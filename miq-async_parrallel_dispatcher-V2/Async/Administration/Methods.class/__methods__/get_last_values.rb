#
# Description: <Method description here>
#

worker=$evm.root["dialog_option_0_worker_ID"].to_i
task=$evm.vmdb(:automation_task,worker) rescue nil
$evm.object["value"]=task.options["_ASYNC_#{task.options[:attrs][:worker_type]}"].to_yaml unless task.nil?
$evm.object['visible'] = !task.nil?
