#
# Description: <Method description here>
#

$evm.log(:info,"CC- workers found :")
all_tasks=$evm.vmdb(:automation_task).all.select{|task| task.options[:instance_name] == $evm.root["dialog_option_0_worker"] rescue nil && task.created_on > Time.now - 7.days }
$evm.log(:info,"CC- #{all_tasks.count} workers found")
workers=all_tasks.map{|t| ["#{t.id}", "#{t.id} : #{t.get_option("_Worker_election")} #{t.state}"]}.sort.to_h
$evm.log(:info,"CC- #{workers}")
$evm.object["values"]=workers
$evm.object["default_value"]=workers.keys.sort.last
