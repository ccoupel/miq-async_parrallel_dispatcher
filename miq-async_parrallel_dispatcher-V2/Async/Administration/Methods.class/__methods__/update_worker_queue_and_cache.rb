#
# Description: <Method description here>
#

require("yaml")

worker_id=$evm.root["dialog_option_0_worker_ID"]
worker=$evm.vmdb(:automation_task,worker_id)


if $evm.root["dialog_option_0_cache_update"]=="t"
  cache="_cache_Async"
  yml=$evm.root["dialog_option_0_cache_value"]
  value=YAML.load(yml) rescue nil
  $evm.log(:info,"CC - update cache #{cache}=#{value}")
  worker.set_option(cache,value) unless value.nil?
end

if $evm.root["dialog_option_0_queue_update"]=="t"
  queue="_ASYNC_#{worker.options[:attrs][:worker_type]}"
  yml=$evm.root["dialog_option_0_queue_value"]
  value=YAML.load(yml) rescue nil
    
  $evm.log(:info,"CC - update queue #{queue}=#{value} (#{value})")
  worker.set_option(queue,value) unless value.nil?
end

    $evm.root['ae_result']         = 'ok'
    $evm.root['ae_retry_interval'] = '20.seconds'
