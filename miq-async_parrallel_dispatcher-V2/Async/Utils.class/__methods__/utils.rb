module ManageIQ
  module Automate
    module Async
      module Utils
        class Utils
          # task: "provID_name" => {"name", "prov_id", "type", "state", "status", params={}, "return_values"={} }}
          
          NAMESPACE = 'Async'.freeze
          CLASS_NAME = 'Async_Worker'.freeze
          DEFAULT_THROTTLER_ELECTION_POLICY = 'eldest_active'.freeze
          DEFAULT_THROTTLER_TYPE = 'generic'.freeze
          DEFAULT_THROTTLER_TTL = 100
          DEFAULT_THROTTLER_MIN_INTERVAL = 30
          DEFAULT_QUEUE_PREFIX = '_ASYNC_'.freeze
          DEFAULT_TASKS_SCHEDULING_POLICY = 'fifo'.freeze
          DEFAULT_LIMITS_ADJUSTMENT_POLICY = 'skip'.freeze
          DEFAULT_EMS_MAX_RUNNERS = 10
          DEFAULT_PROVISION_TYPE = 'miq_provision'.freeze
          DEFAULT_PROVISION_MAX_EXECUTION_TIME = 12.hours
          CACHE_KEY = '_cache_Async'.freeze
          @pending_tasks = 0
          @tasks = 0
          @active_provs = []
          #########################################################################
          ########## PUBLIC CLIENT METHODS ###############################################
          #########################################################################
          def self.synchronize_tasks_from_worker(queue_type, handle = $evm)
            # update prov queue with all tasks from worker except if task is already precessed and set as complete

            prov=handle.root[handle.root["vmdb_object_type"]]
            queue_name=queue_name(queue_type)
            prov_queue=get_queue_from_prov(queue_type, prov)
            
             handle.log(:info, "CC- [synchronize_tasks_from_worker] prov_queue  #{queue_name}==#{prov_queue.inspect}")

            worker_prov=get_active_worker(queue_type)
            if worker_prov.nil?
              handle.log(:info,"CC- [synchronize_tasks_from_worker] Waiting for a worker to be ready")
              merged_queue=prov_queue
            else
              worker_queue=get_queue_from_prov(queue_type,worker_prov)
              handle.log(:info, "CC- [synchronize_tasks_from_worker] worker_queue #{queue_name}==#{worker_queue.inspect}")
            # no sync from worker if task is complete
              merged_queue=prov_queue.map do |name, task|
                worker_task=worker_queue[name] || task
                merged_task=task if task["state"]=="complete"
                merged_task= worker_task if task["state"]!="complete"
                [name, merged_task]
              end.to_h
              handle.log(:info, "CC- [synchronize_tasks_from_worker] merged_queue #{queue_name}=#{merged_queue}")
              prov.set_option(queue_name,merged_queue)
            end
            merged_queue
          end
          
          def self.add_async_task(name, params={},type=worker_type($evm),  prov=$evm.root["#{DEFAULT_PROVISION_TYPE}".to_sym])      
            task={"name" => name,
                  "type" => type,
                  "prov_id" => prov.id,
                  "state" => "pending",
                  "status" => "ok", 
                  "return_values" => nil, 
                  "params" => params}
            push_task_to_queue(task)
          end
          
          ######### GETters ########
          def self.get_async_task(task_name, type=worker_type($evm),  prov=$evm.root[$evm.root["vmdb_object_type"]])
            #prov=$evm.root[$evm.root["vmdb_object_type"]]
            
            unless prov.nil?
              queue=get_queue_from_prov(type,prov)
              $evm.log(:info,"CC- get_async name=#{task_name} queue=#{queue }")
              $evm.log(:info,"CC- get_async name=#{task_name} task=#{queue["#{task_name}"] rescue "NONE"}")
              $evm.log(:info,"CC- get_async name=#{prov.id}_#{task_name} task=#{queue["#{prov.id}_#{task_name}"] rescue "NONE"}")
              result=queue["#{task_name}"]
              result=queue["#{prov.id}_#{task_name}"] if result.nil?
              result
            end
          end
          
          def self.get_async_task_status(task_name, type=worker_type($evm),  prov=$evm.root[$evm.root["vmdb_object_type"]]) 
            task=get_async_task(task_name, type,  prov)
              task["status"] rescue nil
          end

          def self.get_async_task_state(task_name, type=worker_type($evm),  prov=$evm.root[$evm.root["vmdb_object_type"]])
            task=get_async_task(task_name, type,  prov)
              task["state"] rescue nil
          end     
          
          def self.get_async_task_values(task_name, type=worker_type($evm),  prov=$evm.root[$evm.root["vmdb_object_type"]])
            task=get_async_task(task_name, type,  prov)
              task["return_values"] rescue nil
          end
          ######### SETters ########
          def self.update_task_pending(task_name , values=nil) 
            state='pending'
            status='ok'
            self.update_task(task_name , state, status, values) 
          end
          
          def self.update_task_finished(task_name , values=nil) 
            state='finished'
            status='ok'
            self.update_task(task_name , state, status, values)
          end    
          
          def self.update_task_error(task_name , values=nil) 
            state='error'
            status='ok'            
            self.update_task(task_name , state, status, values)
          end    

          def self.update_task_active(task_name , values=nil) 
            state='active'
            status='ok'            
            self.update_task(task_name , state, status, values)
          end          
          
          def self.update_task_complete(task_name , values=nil) 
            state='complete'
            status='ok'            
            self.update_task(task_name , state, status, values)
          end  
          #########################################################################
          ########## PUBLIC WORKER METHODS ###############################################
          #########################################################################
          
          def self.update_task(task_name , state, status="ok", values=nil) 
            $evm.log(:info,"[update_task_name] try to update #{task_name} to #{state}")
            task=get_async_task(task_name)
            task["status"] = status
            task["state"] = state
            task["return_values"] = values unless values.nil?
            push_task_to_queue(task, "#{task["prov_id"]}_#{task["name"]}")
          end
          
          def self.push_task_to_queue(task, name=nil)
            $evm.log(:info,"CC- [push_task_to_queue] #{task["type"]} : #{task} ")
            task_name=name || "#{task["prov_id"]}_#{task["name"]}"
            prov=$evm.root[$evm.root["vmdb_object_type"]]

            queue=get_queue_from_prov(task["type"], prov)
            queue["#{task_name}"] = task
            prov.set_option(queue_name(task["type"]),queue)
            $evm.log(:info,"CC- [push_task_to_queue] queue #{queue_name(task["type"])} of prov #{task["prov_id"]} updated: #{prov.get_option(queue_name(task["type"]))}")
          end
          
          def self.get_from_cache(key, prov=$evm.root[$evm.root['vmdb_object_type']])
            prov.get_option(CACHE_KEY)[key] || "" rescue ""
          end
          
          def self.add_to_cache(key,value, prov=$evm.root[$evm.root['vmdb_object_type']])
            cache=prov.get_option(CACHE_KEY) || ""
            cache[key]=value
            update_cache(cache)
          end
 
          

          #########################################################################
          ########## PRIVATE METHODS ##############################################
          #########################################################################   
          
          def self.get_NAMESPACE()
            NAMESPACE
          end
          
          def self.get_CLASSNAME()
            CLASS_NAME
          end
          
          def self.provision_type(handle = $evm)
            key = 'provision_type'
            @provision_type ||= handle.get_state_var(key) || handle.object["_#{key}"] || handle.object[key] || handle.root[key] || DEFAULT_PROVISION_TYPE
          end
          
          def self.queue_name(type = nil, handle = $evm)
            @queue_name ||= "#{DEFAULT_QUEUE_PREFIX}#{type || self.worker_type($evm)}"
          end
          
          def self.worker_ttl(handle = $evm)
            key = 'worker_ttl'
            @worker_ttl ||= (handle.get_state_var(key) || handle.object["_#{key}"] || handle.object[key] || handle.root[key] || DEFAULT_THROTTLER_TTL).to_i rescue 0
          end
          

          def self.provision_max_execution_time(handle = $evm)
            key = 'provision_max_execution_time'
            @provision_max_execution_time ||= (handle.get_state_var(key) || handle.object["_#{key}"] || handle.object[key] || handle.root[key] || DEFAULT_PROVISION_MAX_EXECUTION_TIME).to_i rescue 0
          end
          
          def self.worker_type(handle = $evm)
            key = 'worker_type'
            @worker_type ||= handle.get_state_var(key) || handle.object["_#{key}"] || handle.object[key] || handle.root[key] || DEFAULT_THROTTLER_TYPE
            $evm.log(:info, "CC- worker_type=#{@worker_type}")
            @worker_type
          end

          def self.worker_min_interval(handle = $evm)
            key = 'worker_min_interval'
            @worker_min_interval ||= (handle.get_state_var(key) || handle.object["_#{key}"] || handle.object[key] || handle.root[key] || DEFAULT_THROTTLER_MIN_INTERVAL).to_i rescue 0
          end
          
          def self.queue_name(type = nil, handle = $evm)
            @queue_name ||= "#{DEFAULT_QUEUE_PREFIX}#{type || self.worker_type($evm)}"
            @queue_name
          end          
          #########################################################################
          
          def self.get_previous_tasks_values(handle = $evm)
            get_value_from_last_dead_worker(queue_name(worker_type), handle) || {} rescue {}
          end
          
          def self.get_previous_cache_values(handle = $evm)
            get_value_from_last_dead_worker(CACHE_KEY, handle) || {} rescue {}
          end
          
          def self.get_value_from_last_dead_worker(key, handle = $evm)
            last_throttler=dead_workers(worker_type,handle).select{|request| !request.get_option(key).blank? && request.get_option("_Worker_election") == "MASTER" }.sort_by{|request| request.id}
            $evm.log(:info,"    CC- #{last_throttler.count} workers with values for #{key}")
            $evm.log(:info,"    CC- last_worker =  #{last_throttler.first.id}/#{last_throttler.last.id}") unless last_throttler.count == 0
            last_throttler = last_throttler.last rescue nil
            last_throttler.get_option(key) rescue nil
          end
          
          def self.update_cache(cache_values, prov=$evm.root[$evm.root['vmdb_object_type']])
            $evm.log(:info,"  CC- [update_cache] prov=#{prov.attributes}")
            prov.set_option(CACHE_KEY,cache_values)
          end       

          #########################################################################

          def self.get_async_queue(type=worker_type($evm),  prov=nil)
            prov=get_active_worker(type) if prov.nil?
            unless prov.nil?
              get_queue_from_prov(type,prov)
            end
          end
          
          def self.get_queue_from_prov(type, prov)
            $evm.log(:info, "CC- [get_queue_from_prov] prov_queue  #{prov.options}")
            prov.get_option(queue_name(type)) || {} rescue {}
          end
          
          def self.log_and_raise(message, handle = $evm)
            handle.log(:error, message)
            raise "ERROR - #{message}"
          end

          def self.task(handle = $evm)
            @task ||= handle.root['automation_task'].tap do |task|
              log_and_raise('An automation_task is needed for this method to continue', handle) if task.nil?
            end
          end
          
          #########################################################################

          def self.current_worker(handle = $evm)
            @current_worker ||= task(handle).miq_request.tap do |request|
              log_and_raise('A miq_request is needed for this method to continue', handle) if request.nil?
            end
          end

          def self.get_active_worker(type=nil, handle = $evm)
            # return the active worker with the lowest ID
            type = worker_type(handle) if type.nil?
            @active_worker ||= active_workers(type,handle).sort { |worker| worker.id }.first
            $evm.log(:info,"CC- active worker = #{@active_worker.id rescue "NONE"}")

              @active_worker=handle.root[handle.root["vmdb_object_type"]] if @active_worker.id == handle.root[handle.root["vmdb_object_type"]].id rescue nil
            @active_worker

          end
          
          def self.get_all_workers(type=nil, handle = $evm)
            # return all workers, dead and active, for given type
            type = worker_type(handle) if type.nil?
            @all_throttlers ||= handle.vmdb(:automation_task).all.select do |request|
                                          request.request_type == 'automation' &&
                                          request.options[:namespace] == NAMESPACE &&
                                          request.options[:class_name] == CLASS_NAME &&
                                          request.options[:instance_name] == "#{type}_worker" rescue nil
                                        end
            @all_throttlers
          end         
          
          def self.active_workers(type=nil, handle = $evm)
            # return all active worker for given type
            type = worker_type(handle) if type.nil?
            all_workers=get_all_workers(type,handle)
            @active_throttlers ||= all_workers.select { |request| request.state == "active" }
            $evm.log(:info,"CC- active workers = #{@active_throttlers.count}/#{all_workers.count}")
             @active_throttlers
          end

          def self.dead_workers(type=nil, handle = $evm)
            # return all non active worker for given type
            type = worker_type(handle) if type.nil?
            all_workers=get_all_workers(type,handle)
            @dead_throttlers ||= all_workers.select { |request| request.status == "ok" && request.state == "finished" }
            $evm.log(:info,"CC- dead workers = #{@dead_throttlers.count}/#{all_workers.count}")
             @dead_throttlers
          end
          
          def self.set_elected_worker(handle = $evm)
            handle.root[handle.root["vmdb_object_type"]].set_option("_Worker_election","MASTER")
          end

          def self.elected_worker?(handle = $evm)
            # return TRUE if no older worker is running
            active_workers(worker_type,handle).select { |t| 
              $evm.log(:info,"CC- checking  #{t.id} < #{task(handle).id} => #{t.id < task(handle).id}")
              t.id < task(handle).id }.length.zero?
          end
          
          #########################################################################

          def self.launch(handle = $evm)
            $evm.log(:info,"CC- starting new worker #{worker_type(handle)}")
            handle.execute(
              :create_automation_request,
              {
                :namespace     => NAMESPACE,
                :class_name    => CLASS_NAME,
                :instance_name => "#{worker_type(handle)}_worker",
                :user_id       => handle.vmdb(:user).find_by(:userid => 'admin').id,
                :attrs         => { :worker_type=> worker_type(handle),
                                    :provision_type=> provision_type(handle),
                                    :worker_ttl => worker_ttl(handle),
                                    :worker_min_interval => worker_min_interval(handle),
                                    :provision_max_execution_time => provision_max_execution_time(handle)}
              },
              'admin',
              true
            )
          end

          def self.retry_or_die(handle = $evm, short_time=false)
            #return if active_provision_tasks(handle).empty?
            delay=(worker_ttl(handle).to_i / handle.root['ae_state_max_retries'].to_i).seconds
            handle.root['ae_retry_interval'] = delay
            handle.root['ae_retry_interval'] = worker_min_interval.seconds if handle.root['ae_retry_interval'] < worker_min_interval.seconds
            

            is_ttl_done = ((Time.now.utc - current_worker(handle).created_on.utc) > worker_ttl(handle).to_i)
            is_count_done=(handle.root['ae_state_max_retries'].to_i == handle.root['ae_state_retries'].to_i)
            handle.root['ae_result'] = 'retry'
            handle.root['ae_result'] = 'ok' if (is_ttl_done || is_count_done)

            handle.log(:info,"CC- worker state : TTL done: #{is_ttl_done} (#{Time.now.utc - current_worker(handle).created_on.utc}/#{worker_ttl(handle).to_i}) COUNT done: #{is_count_done} (#{handle.root["ae_state_retries"]} / #{handle.root['ae_state_max_retries']})")
            handle.log(:info,"CC- worker set to '#{handle.root['ae_result']}' with #{handle.root['ae_retry_interval']} seconds (originaly #{delay})") 
            # on relance le worker tanyt que des provisionings sont en cours
            
            #handle.root['ae_result'] = 'ok'
            launch(handle) if handle.root['ae_result'] == 'ok' && @active_provs.count > 0
          end

          #########################################################################
          
          def self.schedule_tasks(handle = $evm)
            unassigned_async_tasks(handle).sort_by { |name, task| task["prov_id"] } rescue {}
          end

          #########################################################################     

          def self.synchronize_tasks_from_prov(handle = $evm)
            # update worker queue with all pending tasks from provs
            # remove from worker queue tasks from inactive provs
            # remove from worker queue completed tasks
            worker_prov=handle.root[$evm.root["vmdb_object_type"]]
            worker_queue=get_queue_from_prov(worker_type, worker_prov)
            
            prov_queue=unassigned_async_tasks_from_provs(handle)
            
            #add to worker queue new tasks
            queue=worker_queue.merge(prov_queue){ |key, worker_val, prov_val| worker_val}
            
            # remove from worker_queue finished and error prov
            handle.log(:info, "CC- #{queue.count} tasks from worker queue")
            clean_queue=queue.map  do |name, worker_task|
              prov=$evm.vmdb(provision_type,worker_task["prov_id"])
              handle.log(:info, "CC- is prov #{worker_task["prov_id"]} finished? #{prov.state}/#{prov.status}")
                [name , worker_task] unless prov.state == "finished" || prov.state == "error"
            end.select { |task| !task.nil?}.to_h
            worker_prov.set_option(queue_name,clean_queue)
            handle.log(:info, "    CC- worker active prov #{clean_queue}")
            
            #remove prov completed tasks
            final_queue=clean_queue.map  do |name, worker_task|
              prov=$evm.vmdb(provision_type,worker_task["prov_id"])
                prov_queue=get_queue_from_prov(queue_name,prov)
                prov_task=prov_queue[name]
                handle.log(:info, "    CC- is task #{name} complete? #{prov_task["state"]}")
                [name , worker_task] if prov_task["state"]!="complete"
            end.select { |task| !task.nil?}.to_h
            handle.log(:info, "    CC- worker final active tasks #{final_queue}")
            final_queue
          end
          
          def self.unassigned_async_tasks(handle = $evm)
            # get all pending tasks from worker
            all_worker_tasks=synchronize_tasks_from_prov(handle)
           handle.log(:info, "CC- worker_tasks=#{all_worker_tasks}")
            unassigned_tasks=all_worker_tasks.select { |name, task| task["state"] == "pending"}  
            handle.log(:info, "CC- #{unassigned_tasks.count}/#{all_worker_tasks.count} pending tasks found (#{unassigned_tasks})")
            unassigned_tasks
          end
          
          def self.unassigned_async_tasks_from_provs(handle = $evm)
            @pending_tasks = all_async_tasks(handle).select { |k,t| t["state"] == "pending" } rescue {}
            handle.log(:info, "CC- #{@pending_tasks.count} pending tasks found in provs")
            @pending_tasks
          end
          
          def self.all_async_tasks(handle = $evm)
            @tasks = active_provision_tasks(handle)
                                  .select {|prov|   prov.get_option(queue_name) }
                                  .collect { |prov| prov.get_option(queue_name) }
            @tasks=@tasks.flat_map(&:to_a).to_h
            handle.log(:info, "CC- #{@tasks.count} tasks found for #{queue_name} in provs")
            @tasks
          end
          
          def self.active_provision_tasks(handle = $evm)
            #handle.vmdb(:service_template_provision_task)            #.where(:state => 'active')
            # select only active provs for less than 2h 
            @active_provs=handle.vmdb(provision_type).all.select do |prov| 
                  true if prov.state != "finished" && prov.status != "error" && prov.created_on > Time.now - provision_max_execution_time
            end
            handle.log(:info, "CC- #{@active_provs.count} active provs found for #{provision_type}")
            @active_provs
          end

        end
      end
    end
  end
end
