module ManageIQ
  module Automate
    module Async
      module Workers
        class Generic_worker
          def initialize(handle = $evm)
            @handle = handle
          end

          def manage_active_task(request)
             $evm.log(:info, "CC -manage #{name} => #{request}")
             ManageIQ::Automate::Async::Utils::Utils.update_task_active(request)
             do_something(request)
          end
          
          def do_something(request)
            x=ManageIQ::Automate::Async::Utils::Utils.get_from_cache("key_test") || 0
            ManageIQ::Automate::Async::Utils::Utils.add_to_cache("key_test",x.to_i+1)
            ManageIQ::Automate::Async::Utils::Utils.update_task_finished(request , x)
          end
          
          def main
            return unless ManageIQ::Automate::Async::Utils::Utils.elected_worker?(@handle)
            ManageIQ::Automate::Async::Utils::Utils.set_elected_worker(@handle)
            tasks=ManageIQ::Automate::Async::Utils::Utils.schedule_tasks(@handle)
            tasks.each { |name, task| manage_active_task(name, task)}
            
            ManageIQ::Automate::Async::Utils::Utils.retry_or_die(@handle, tasks.count!=0)
          end
        end
      end
    end
  end
end

ManageIQ::Automate::Async::Workers::Generic_worker.new.main
