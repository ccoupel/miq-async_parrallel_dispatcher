module ManageIQ
  module Automate
    module Async
      module Workers
        class Initialize

          def initialize(handle = $evm)
            @handle = handle
            @handle.root.attributes.each { |k,v| @handle.log(:info,"CC- init ROOT[#{k}]=#{v}"); @handle.set_state_var(k,v) rescue nil}
           
            cache = ManageIQ::Automate::Async::Utils::Utils.get_previous_cache_values(@handle)
            @handle.log(:info,"CC- [initialize] previous cache values: #{cache}")
            ManageIQ::Automate::Async::Utils::Utils.update_cache(cache, handle.root[handle.root['vmdb_object_type']]) 
            
            tasks = ManageIQ::Automate::Async::Utils::Utils.get_previous_tasks_values(@handle)
             @handle.log(:info,"CC- [initialize] previous queue values: #{tasks}")
            ManageIQ::Automate::Async::Utils::Utils.get_active_worker(ManageIQ::Automate::Async::Utils::Utils.worker_type,@handle).set_option(ManageIQ::Automate::Async::Utils::Utils.queue_name(ManageIQ::Automate::Async::Utils::Utils.worker_type),tasks)

          end

          def main
#$evm.root["ae_result"]="retry"
          end
        end
      end
    end
  end
end

ManageIQ::Automate::Async::Workers::Initialize.new.main
