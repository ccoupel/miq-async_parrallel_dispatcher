module ManageIQ
  module Automate
    module Async
      module Workers
        class Start_Worker

          def initialize(handle = $evm)
            @handle = handle
#            @handle.root.attributes.each { |k,v| $evm.log(:info,"CC- Start_Worker ROOT[#{k}]=#{v}")}
 #           @handle.object.attributes.each { |k,v| $evm.log(:info,"CC- Start_Worker OBJECT[#{k}]=#{v}")}
#            @active_workers = ManageIQ::Automate::Async::Utils::Utils.active_workers(@handle)
          end
=begin
          def new_worker(type, handle = $evm)
            $evm.log(:info,"CC- starting new worker #{type}")
            handle.execute(
              :create_automation_request,
              {
                :namespace     => ManageIQ::Automate::Async::Utils::Utils.get_NAMESPACE,
                :class_name    => ManageIQ::Automate::Async::Utils::Utils.get_CLASSNAME,
                :instance_name => "#{handle.root["_missing_instance"]}",
                :user_id       => handle.vmdb(:user).find_by(:userid => 'admin').id,
                :attrs         => { :worker_type=> type, :provision_type=> ManageIQ::Automate::Async::Utils::Utils.provision_type(handle) }
              },
              'admin',
              true
            )
          end
=end
          def main
            worker_type=ManageIQ::Automate::Async::Utils::Utils.worker_type(@handle)
              @active_workers = ManageIQ::Automate::Async::Utils::Utils.active_workers(worker_type, @handle)
              ManageIQ::Automate::Async::Utils::Utils.launch(@handle) if @active_workers.empty?
              @handle.log(:info,"Worker #{worker_type} is already running with #{@active_workers.count} instances") unless @active_workers.empty?
          end
        end
      end
    end
  end
end

ManageIQ::Automate::Async::Workers::Start_Worker.new.main
